Source: python-matplotlib-venn
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               pybuild-plugin-pyproject,
               python3,
               python3-setuptools,
               python3-matplotlib,
               python3-shapely,
               python3-scipy,
               python3-pytest,
               python3-tk,
               python3-pil.imagetk,
               xauth <!nocheck>,
               xvfb <!nocheck>
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-matplotlib-venn
Vcs-Git: https://salsa.debian.org/python-team/packages/python-matplotlib-venn.git
Homepage: https://github.com/konstantint/matplotlib-venn
Rules-Requires-Root: no

Package: python3-matplotlib-venn
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-tk
Description: Python 3 plotting area-proportional two- and three-way Venn diagrams
 Matplotlib is a pure Python plotting library designed to bring
 publication quality plotting to Python with a syntax familiar to
 Matlab users. All of the plotting commands in the pylab interface can
 be accessed either via a functional interface familiar to Matlab
 users or an object oriented interface familiar to Python users.
 .
 This module provides routines for plotting area-weighted two- and
 three-circle venn diagrams.
 .
 This package contains the Python 3 version of matplotlib.
